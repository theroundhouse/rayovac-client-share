
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png" />
        <title>Rayovac</title>
        <link rel='stylesheet' id='app-css'  href='/dist/css/app.min.css' type='text/css' media='all' />
        <link rel='stylesheet' id='app-css'  href='/dist/css/rayovac.css' type='text/css' media='all' />
    </head>
    <body>
        <header role="banner" id="banner">
            <div class="inner">
                <a href="#" class="logo">Rayovac</a>
                <!-- END .logo -->
                <a id="nav-primary-toggle" href="#nav-primary"><span>Toggle navigation</span><em>Menu</em></a>
                <a id="nav-secondary-toggle" href="#nav-secondary" class="secondary-toggle">
                    <span></span>
                    <em>Menu</em>
                </a>
                <nav id="nav-primary" class="menu-retail-container">
                    <ul id="menu-retail" class="menu">
                        <li class="basket-link"><a href="template-get-started.php">Basket</a></li>
                        <li class="customerservice-link"><a href="template-m.php">Customer Service</a></li>
                        <li class="my-account-link"><a href="template-b.php">My Account</a></li>
                        <li><a href="template-d.php">GBP (£)</a></li>
                        <li class="language-link gb">
                            <a href="template-a2.php"><img src="dist/img/icons/gb-icon.png">United Kingdom</a>
                        </li>
                    </ul>
                </nav>
            </div>


            <div id="nav-secondary" class="menu-main-container">
                <ul id="menu-retail" class="menu">
                    <li><a href="template-get-started.php">Get Started</a></li>
                    <li data-menu="shop"><a href="template-d.php">Shop</a></li>
                    <li data-menu="products"><a href="template-d.php">Products</a></li>

                    <li>
                        <a href="template-m.php">Discover</a>
                        <ul>
                            <li>
                                <a href="template-h.php">Long lasting batteries?</a>
                                <ul>
                                    <li><a href="template-h.php">Level 3 a</a></li>
                                    <li><a href="template-h.php">Level 3 b</a></li>
                                    <li><a href="template-h.php">Level 3 c</a></li>
                                </ul>
                            </li>
                            <li><a href="template-h.php">Environment</a></li>
                            <li><a href="template-h.php">Joy of hearing</a></li>
                        </ul>
                    </li>
                    <li><a href="template-b.php">Charity</a></li>
                    <li onclick="void(0);">
                        <a href="template-h.php">Insights</a>
                        <ul>
                            <li><a href="template-h.php">Did you know?</a></li>
                            <li><a href="template-h.php">Seasonal info</a></li>
                            <li><a href="template-h.php">News &amp; media</a></li>
                        </ul>
                    </li>
                    <li class="other-section"><a href="template-a2.php">Trade customers</a></li>
                </ul>
            </div>

            <div id="local-selector">
                <div class="inner">
                    <div>
                        <p>Select your location</p>
                    </div>
                    <button class="btn-close"></button>
                    <ul>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_0" class="local-int" href="http://rayovac.eu/en/"><span id="ctl02_lvLanguageSelection_AspLabelText_0">International</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_1" class="local-be" href="/fr-be"><span id="ctl02_lvLanguageSelection_AspLabelText_1">Belgique</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_2" class="local-de" href="http://rayovac.eu/de-DE/"><span id="ctl02_lvLanguageSelection_AspLabelText_2">Deutsch</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_3" class="local-at" href="/de-at"><span id="ctl02_lvLanguageSelection_AspLabelText_3">Deutsch (AT)</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_4" class="local-au" href="/en-au"><span id="ctl02_lvLanguageSelection_AspLabelText_4">English (AU)</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_5" class="local-nz" href="/en-au"><span id="ctl02_lvLanguageSelection_AspLabelText_5">English (NZ)</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_6" class="local-en" href="/en-gb"><span id="ctl02_lvLanguageSelection_AspLabelText_6">English (UK)</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_7" class="local-us" href="/en-us"><span id="ctl02_lvLanguageSelection_AspLabelText_7">English (US)</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_8" class="local-es" href="http://rayovac.eu/es-ES/"><span id="ctl02_lvLanguageSelection_AspLabelText_8">Español</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_9" class="local-fr" href="http://rayovac.eu/fr-FR/"><span id="ctl02_lvLanguageSelection_AspLabelText_9">Français</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_10" class="local-hi" href="http://rayovac.in/hi-IN/"><span id="ctl02_lvLanguageSelection_AspLabelText_10">हिन्दी</span></a>
                        </li>

                        <li>
                            <a id="ctl02_lvLanguageSelection_AspHyperlink_11" class="local-it" href="http://rayovac.eu/it-IT/"><span id="ctl02_lvLanguageSelection_AspLabelText_11">Italiano</span></a>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="nav-sub-menu" id="" data-menu="shop">
                <ul id="nav-sub-menu-products">
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #f8e118;">
                                <img src="dist/img/media/product-1.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayovac Proline Advanced</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #f77000;">
                                <img src="dist/img/media/product-2.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayoval Extra Advanced</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #914700;">
                                <img src="dist/img/media/product-3.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Implant Pro+</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #1fafe7;">
                                <img src="dist/img/media/product-4.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Retail</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #acb0b6;">
                                <img src="dist/img/media/product-1.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayovac Proline Advanced</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #acb0b6;">
                                <img src="dist/img/media/product-2.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayovac Extra Advanced</em>
                        </a>
                    </li>
                </ul>
           </div>            
            <div class="nav-sub-menu" id="" data-menu="products">
                <ul id="nav-sub-menu-products">
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #f8e118;">
                                <img src="dist/img/media/product-1.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayovac Proline Advanced</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #f77000;">
                                <img src="dist/img/media/product-2.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayoval Extra Advanced</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #914700;">
                                <img src="dist/img/media/product-3.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Implant Pro+</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #1fafe7;">
                                <img src="dist/img/media/product-4.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Retail</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #acb0b6;">
                                <img src="dist/img/media/product-1.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayovac Proline Advanced</em>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="img-holder" style="background-color: #acb0b6;">
                                <img src="dist/img/media/product-2.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Rayovac Extra Advanced</em>
                        </a>
                    </li>
                </ul>
           </div>
        </header>
        <!-- END header -->
        <main>
