
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <title>Rayovac</title>
        <link rel='stylesheet' id='app-css'  href='/dist/css/app.min.css' type='text/css' media='all' />
    </head>
    <body class="home page page-id-2 page-template page-template-template-main-home page-template-template-main-home-php">
        <header role="banner" id="banner">
            <div class="inner">
                <a href="#" class="logo">Rayovac</a>
                <!-- END .logo -->
                <a id="nav-primary-toggle" href="#nav-primary"><span>Toggle navigation</span><em>Menu</em></a>
                <nav id="nav-primary" class="menu-trade-container">
                    <ul id="menu-trade" class="menu">
                        <li data-menu="products" ><a href="#">Products</a></li>
                        <li>
                            <a href="#">Company</a>
                            <ul class="sub-menu">
                                <li><a href="http://rayovac-trade.roundhouseinteractive.net/company/corporate-social/">Corporate &#038; Social</a></li>
                                <li><a href="http://rayovac-trade.roundhouseinteractive.net/company/environmental-policy/">Environmental Policy</a></li>
                                <li><a href="http://rayovac-trade.roundhouseinteractive.net/company/history/">History</a></li>
                                <li><a href="http://rayovac-trade.roundhouseinteractive.net/company/locations/">Locations</a></li>
                                <li><a href="http://rayovac-trade.roundhouseinteractive.net/company/spectrum-brands/">Spectrum Brands</a></li>
                                <li><a href="http://rayovac-trade.roundhouseinteractive.net/company/charity/">Charity</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Innovation</a></li>
                        <li><a href="#">Media Centre</a></li>
                        <li><a href="#">Charity</a></li>
                        <li><a href="#">Retail Customers</a></li>
                    </ul>
                </nav>
            </div>
            <div id="nav-secondary">
                <ul>
                    <li>
                        <a href="#">English</a>
                        <ul class="lng">
                            <li rel="de"><a href="#">Deutch</a></li>
                        </ul> 
                    </li>
                    <li>
                        <a href="#">GBP (&pound;)</a>
                        <ul class="currency">
                            <li rel="EUR"><a href="#">EUR (&euro;)</a></li>
                        </ul> 
                    </li>
                    <li>
                        <a href="">Contact Us</a>
                    </li>
                    <li>
                        <a href="">Careers</a>
                    </li>
                    <li>
                        <a href="">Retail customers</a>
                    </li>
                </ul>
            </div>  
            <div class="nav-sub-menu" id="nav-sub-trade" data-menu="products" >
                <ul id="nav-sub-menu-products">
                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-proline/">
                            <div class="img-holder" style="background-color: #b3944a;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/10/trade-product-image-proline-64x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>ProLine</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-extra-advanced/">
                            <div class="img-holder" style="background-color: #a7a9ac;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/10/Rayovac-Extra-Prem-Pack-6pk-size-10-71x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Extra</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-retail/">
                            <div class="img-holder" style="background-color: #003263;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/10/60723-Rayovac-Retail-13-6-pack-66x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Retail</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-implant-pro-plus/">
                            <div class="img-holder" style="background-color: #c2c5cc;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/10/ZAP-Cochlear-MF-Implant-pro-6pk-675-Front-64x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Implant Pro+</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-peak/">
                            <div class="img-holder" style="background-color: #bd9c62;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/10/PEAK-6pk-10-2015.05-73x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Peak</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-alkaline/">
                            <div class="img-holder" style="background-color: #44acdc;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/10/04006944404_14701_20151001-93x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Alkaline</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-quartz/">
                            <div class="img-holder" style="background-color: #fe8e00;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/10/60723-Rayovac-Retail-13-6-pack-354x755-1-98x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Quartz</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/rayovac-lithium/">
                            <div class="img-holder" style="background-color: #7399b5;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/11/Lithium-74x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Lithium</em>
                        </a>
                    </li>

                    <li>
                        <a href="http://rayovac-trade.roundhouseinteractive.net/products/accessories/">
                            <div class="img-holder" style="background-color: #dd3333;">
                                <img src="http://rayovac-trade.roundhouseinteractive.net/wp-content/uploads/2016/11/Rayovac-Battery-Holder-61x140.png" />
                                <div class="overlay"></div>
                            </div>
                            <em>Accessories</em>
                        </a>
                    </li>

                </ul>
            </div>           
        </header>
        <!-- END header -->
        <main>