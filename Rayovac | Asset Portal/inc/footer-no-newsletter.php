</main>
<!-- END main -->

<footer>
    <div class="inner">
        <div id="corp">
            <a href="#" class="logo-spectrum">part of Spectrum Brands</a>
        </div>
        <div id="social">
            <h4>Stay Connected</h4>
            <a href="#" class="btn-social btn-facebook">like us on facebook</a>
            <a href="#" class="btn-social btn-twitter">follow us on twitter</a>
            <a href="#" class="btn-social btn-linkedin">our linkedin page</a>
            <a href="#" class="btn-social btn-youtube">our youtube channel</a>
        </div>

        <div id="side" class="site-map">
            <ul id="menu-footer-en" class="menu">
                <li><a href="#">Consumer</a>
                    <ul class="sub-menu">
                        <li><a href="#">Shop</a></li>
                        <li><a href="#">Trade Homepage</a></li>
                        <li><a href="#">Innovation</a></li>
                        <li><a href="#">Company</a></li>
                        <li><a href="#">Insights</a></li>
                    </ul>
                </li>
                <li><a href="#">Customer Services</a>
                    <ul class="sub-menu">
                        <li><a href="#">Delivery</a></li>
                        <li><a href="#">Returns</a></li>
                        <li><a href="#">FAQs</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </li>
                <li><a href="#">Site Information</a>
                    <ul class="sub-menu">
                        <li><a href="#">Legal</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms &#038; Conditions</a></li>
                        <li><a href="#">Cookie Policy</a></li>
                        <li><a href="#">Sitemap</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <em>&copy; Spectrum Brands, Inc.<br>All Rights Reserved</em>
    </div>
    <div class="inner sub-footer">
        <p>(1) Based on internal company estimates/based on worldwide marketshare</p>
        <p>(2) Exclusions apply. Orders qualifying for 24 hour shipment contain only ProLine branded products, are placed before 12 noon and contain 6000 cells or less. Orders excluded from the 24 hour shipping guarantee include promotional orders, where there are any account issues such as customer credit, pricing or custom imprint changes. New customer set ups will not qualify for 24 hour shipping for their first order. 24 hour shipping does not apply when there are National holidays. See website for tull T&Cs</p>
        <p>(3) Based on an average calculation from 2013 - 2015</p>
        <p>(4) Compared to previous tab design</p>
        <p>(5) Required by law</p>
        <p>6) We will replace, at our discretion, any device damaged by a Rayovac battery, if handled correctly, before the best before date and sent pre-paid to your local Rayovac office</p>
        <p>(7) Limit 1 per household</p>
    </div>
</footer>
<!--[if gte IE 9]><!-->
<!-- Using version 1 for consistency with IE8 version in <head> -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!--<![endif]-->

<script type='text/javascript' src='/dist/js/plugins.min.js?ver=4.5.3'></script>
<script type='text/javascript' src='/dist/js/main.min.js?ver=4.5.3'></script>

</body>
</html>
