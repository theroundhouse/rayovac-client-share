<div class="featured-insights" >
    <div class="featured-insights-header" >
        <em>Media Centre</em>
        <a href="" class="cta">View all news &amp; events</a>
    </div>
    <div class="featured-insights-pane" >
        <div class="posts-sticky">
            <article>
                <div class="img-holder"><img width="1440" height="826" src="uploads/2016/06/trade_slide_mtb.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="trade_slide_mtb" srcset="uploads/2016/06/trade_slide_mtb.jpg 1440w, uploads/2016/06/trade_slide_mtb-300x172.jpg 300w, uploads/2016/06/trade_slide_mtb-768x441.jpg 768w, uploads/2016/06/trade_slide_mtb-1024x587.jpg 1024w, uploads/2016/06/trade_slide_mtb-402x230.jpg 402w, uploads/2016/06/trade_slide_mtb-830x476.jpg 830w" sizes="(max-width: 1440px) 100vw, 1440px" /></div>
                <h3><a href="#">A third blog test</a></h3>
                <a href="#" class="cta">Read more</a>
            </article>
            <article>
                <div class="img-holder"><img width="1440" height="826" src="uploads/2016/06/trade_slide_headphones.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="trade_slide_headphones" srcset="uploads/2016/06/trade_slide_headphones.jpg 1440w, uploads/2016/06/trade_slide_headphones-300x172.jpg 300w, uploads/2016/06/trade_slide_headphones-768x441.jpg 768w, uploads/2016/06/trade_slide_headphones-1024x587.jpg 1024w, uploads/2016/06/trade_slide_headphones-402x230.jpg 402w, uploads/2016/06/trade_slide_headphones-830x476.jpg 830w" sizes="(max-width: 1440px) 100vw, 1440px" /></div>
                <h3><a href="#">A second blog example</a></h3>
                <a href="#" class="cta">Read more</a>
            </article>
            <article>
                <div class="img-holder"><img width="1440" height="741" src="uploads/2016/05/McLaren-P1-Bahrain-773-crop5184x2670.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="McLaren P1 Bahrain-773-crop5184x2670" srcset="uploads/2016/05/McLaren-P1-Bahrain-773-crop5184x2670.jpg 1440w, uploads/2016/05/McLaren-P1-Bahrain-773-crop5184x2670-300x154.jpg 300w, uploads/2016/05/McLaren-P1-Bahrain-773-crop5184x2670-768x395.jpg 768w, uploads/2016/05/McLaren-P1-Bahrain-773-crop5184x2670-1024x527.jpg 1024w, uploads/2016/05/McLaren-P1-Bahrain-773-crop5184x2670-830x427.jpg 830w" sizes="(max-width: 1440px) 100vw, 1440px" /></div>
                <h3><a href="#">A blog example</a></h3>
                <a href="#" class="cta">Read more</a>
            </article>
        </div>
        <div class="posts-recent">
            <h3>More recent posts...</h3>
            <article>
                <h4><a href="#">Helping to keep your customers happy and loyal</a></h4>
            </article>
            <article>
                <h4><a href="#">Proline 3 launched across europe</a></h4>
            </article>
            <article>
                <h4><a href="#">New media centre launched for Rayovac</a></h4>
            </article>
        </div>
    </div>
</div>