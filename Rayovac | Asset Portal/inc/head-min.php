
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <title>Rayovac</title>
        <link rel='stylesheet' id='app-css'  href='/dist/css/app.min.css' type='text/css' media='all' />
    </head>
    <body class="home page page-id-2 page-template page-template-template-main-home page-template-template-main-home-php">
        <header role="banner" id="banner">
            <div class="inner">
                <div id="nav-secondary" class="light">
                    <ul>
                        <li>
                            <a href="#">English</a>
                            <ul class="lng">
                                <li rel="de"><a href="#">Deutch</a></li>
                            </ul> 
                        </li>
                    </ul>
                </div>  
            </div>      
        </header>
        <!-- END header -->
        <main>