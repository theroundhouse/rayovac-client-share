<?php include 'inc/head.php'; ?>
    <div id="asset-portal">
        <div class="inner">

            <div class="asset-portal__intro">
                <h1>Asset Portal</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vel sapien tellus lorem ipsum dolor
                    sit.
                </p>
            </div>

            <div class="asset-list">
                <ul class="asset-list asset-list__primary">
                    <li><p>Retail <em class="subnav-link" data-ref="shop"></em></p></li>
                    <li>
                        <p>Extra</p>
                        <ul class="asset-list asset-list__secondary">
                            <li>
                                <div class="asset-grid asset-grid--always-show">
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Rayovac_Extra_logo.png"/>
                                            </div>
                                            <span>Rayovac-Extra_Advanced-Logo</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <p>4 Pack</p>
                                <div class="asset-grid">
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size10_Flat.png"/>
                                            </div>
                                            <span>Size 10 - Flat - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size13_Flat.png"/>
                                            </div>
                                            <span>Size 13 Flat - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size312_Flat.png"/>
                                            </div>
                                            <span>Size 312 Flat - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size675_Flat.png"/>
                                            </div>
                                            <span>Size 675 Flat - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Group_AngleLeft_ActualSize.png"/>
                                            </div>
                                            <span>Group - Angled Left - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Group_AngleRight_ActualSize.png"/>
                                            </div>
                                            <span>Group - Angled Right - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Group_Cascade.png"/>
                                            </div>
                                            <span>Group - Cascade - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Group_Flat.png"/>
                                            </div>
                                            <span>Group - Flat - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Group_Row_Size10Front_ActualSize.png"/>
                                            </div>
                                            <span>Group - Row - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Group_Stack_ActualSize.png"/>
                                            </div>
                                            <span>Group - Stack - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size10_AngleLeft_ActualSize.png"/>
                                            </div>
                                            <span>Size 10 - Angled Left - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size10_AngleRight_ActualSize.png"/>
                                            </div>
                                            <span>Size 10 - Angled Right - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size10_LeanBack_ActualSize.png"/>
                                            </div>
                                            <span>Size 10 - Lean Back - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size10_Stack_ActualSize.png"/>
                                            </div>
                                            <span>Size 10 - Stack - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size13_AngleLeft_ActualSize.png"/>
                                            </div>
                                            <span>Size 13 - Angled Left - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size13_AngleRight_ActualSize.png"/>
                                            </div>
                                            <span>Size 13 - Angled Right - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size13_LeanBack_ActualSize.png"/>
                                            </div>
                                            <span>Size 13 - Lean Back - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size13_Stack_ActualSize.png"/>
                                            </div>
                                            <span>Size 13 - Stack - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size312_AngleLeft_ActualSize.png"/>
                                            </div>
                                            <span>Size 312 - Angled Left - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size312_AngleRight_ActualSize.png"/>
                                            </div>
                                            <span>Size 312 - Angled Right - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size312_LeanBack_ActualSize.png"/>
                                            </div>
                                            <span>Size 312 - Lean Back - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size312_Stack_ActualSize.png"/>
                                            </div>
                                            <span>Size 312 - Stack - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size675_AngleLeft_ActualSize.png"/>
                                            </div>
                                            <span>Size 675 - Angled Left - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size675_AngleRight.png"/>
                                            </div>
                                            <span>Size 675 - Angled Right - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size675_LeanBack_ActualSize.png"/>
                                            </div>
                                            <span>Size 675 - Lean Back - 4 Pack</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Extra_4pk_Size675_Stack_ActualSize.png"/>
                                            </div>
                                            <span>Size 675 - Stack - 4 Pack</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li><p>6 Pack</p></li>
                            <li><p>7+1 Pack</p></li>
                            <li><p>8 Pack</p></li>
                            <li><p>12 Pack</p></li>
                            <li>
                                <p>Brochure</p>

                                <div class="asset-grid">
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper">
                                                <img src="/dist/img/logo/Rayovac_Extra_Brochure_2018_v5.png" />
                                            </div>
                                            <span>66275 Rayovac EXTRA Brochure 2018 EN Europe v5</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <p>Lifestyle Imagery</p>

                                <div class="asset-grid">
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper asset-grid__wrapper--full">
                                                <img class="fill-wrapper" src="/dist/img/logo/IMG-445684.png"/>
                                            </div>
                                            <span>IMG - 445684</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper asset-grid__wrapper--full">
                                                <img class="fill-wrapper" src="/dist/img/logo/IMG-004768.png"/>
                                            </div>
                                            <span>IMG - 004768</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper asset-grid__wrapper--full">
                                                <img class="fill-wrapper" src="/dist/img/logo/IMG-234112.png"/>
                                            </div>
                                            <span>IMG - 234112</span>
                                        </a>
                                    </div>
                                    <div class="asset-gird__block">
                                        <a href="#" download>
                                            <div class="asset-grid__wrapper asset-grid__wrapper--full">
                                                <img src="/dist/img/logo/IMG-100345.png"/>
                                            </div>
                                            <span>IMG - 100345</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><p>Implant Pro +</p></li>
                    <li><p>Alkaline</p></li>
                    <li><p>Quartz</p></li>
                    <li><p>Lithium</p></li>
                    <li><p>Accessories</p></li>
                </ul>
            </div>

        </div>
    </div>
<?php include 'inc/footer-no-newsletter.php'; ?>